#!/usr/bin/env python
#Autor: Maciej Bendec
#self check bo mam 2 rozne wersje pythona
import sys

print(sys.version) 
# ZADANIE 3.1
print "ZADANIE 3.1"
#OK
x = 2 ; y = 3 ;
if (x > y):
    result = x;
else:
    result = y;

print(result)

#Nie dziala, zla skladnia
#for i in "qwerty": if ord(i) < 100: print i
#OK
for i in "axby": print ord(i) if ord(i) < 100 else i

#ZADANIE 3.2
print "ZADANIE 3.2"

#OK
L = [3, 5, 4] ; L = L.sort()
#Zbyt duzo po prawej stronie
#x, y = 1, 2, 3
#Zla skladnia, to krotka nie tablica
#X = 1, 2, 3 ; X[1] = 4
#Out of bounds
#X = [1, 2, 3] ; X[3] = 4
#To string a nie tablica
#X = "abc" ; X.append("d")
#zbyt malo argumentow
#map(pow, range(8))

#ZADANIE 3.3
print "ZADANIE 3.3"

for i in range(0,30):
	if(i%3!=0):
		print(i)

