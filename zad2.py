#!/usr/bin/env python
#Autor: Maciej Bendec
#self check bo mam 2 rozne wersje pythona
import sys

print(sys.version) 
# ZADANIE 2.10
line=""" Bardzo dlugi ciag wyrazow,
tak dlugi ze az szkoda pisac
tyle recznie, ciekawe ile ma wyrazow?
A dla zadania potrzebuje GvR Gv R GVR gvr"""

print(len(line.split()))

# ZADANIE 2.11

word="wyraz"
print("_".join(word))

# ZADANIA 2.12 2.13 2.14
first=""
last=""
longest=""
suma=0
for i in line.split():
	first=first+i[0]
	last=last+i[-1]
	suma=suma+len(i)
	if(len(longest)<len(i)):
		longest=i 
print(first,last)
print(suma)
print(longest,len(longest))
#ZADANIE 2.15
L=[1,2,1351,3,5,2,1,8,3,7]
print ''.join(str(i) for i in L)
#ZADANIE 2.16
print line.replace("GvR","Guido van Rossum")
#ZADANIE 2.17
print sorted(line.split())
print sorted(line.split(),key=len)
#ZADANIE 2.18
longint=1032890124702157031557039175301750317502137531750135701357
print str(longint).count('0')
#ZADANIE 2.19
L=[1,23,54,67,213,57,12,8,3,7,21,85,12,99,125]
print (', '.join(str(i).zfill(3) for i in L))
